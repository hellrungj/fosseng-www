---
---

<div class = 'text-center'>
  <iframe src="https://docs.google.com/presentation/d/1EWUW-wAXHANaoVjaDPyyT1SI0s4B4_JD3LOSWw8eWrI/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</div>

## Things To Do in Computing

* Documentation
* User interface design
* Code review
* Logos and branding
* Translation
* Reproducing bugs
* Quality assurance
* Sysadmining
* Website management
* Legal advice
* User support
* Publicity
* Testing
* Community management
* Creating tutorials
* ... and, yes, coding.
