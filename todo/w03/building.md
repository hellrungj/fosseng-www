---
---

This is the bare skeleton of what we'll do in class Monday.

## Build "Hello, World!"

Fetch the following code to somewhere in your home directory. Hopefully, somewhere that makes sense given the directories you laid out earlier.

[https://github.com/jadudm/hello-world](https://github.com/jadudm/hello-world)

To build this software, you will need to do the following:

1. Use git to check out the software from the repository on GitHub.

1. Learn how to build a C program laid out in the "standard" manner.

1. Learn how to run a program you have successfully built.

To install this software, you need to learn how to convince it not to install to the global installation directory (to which you do not have permissions). You should convince it to install to 

~/usr

instead of 

/usr

All of the information you need for building this program and installing it to the 'usr' directory underneath your own account is somewhere in the package itself.
