---
---

*Inspired by [CS 490 Homework 2 by Heidi Ellis](http://mars.wne.edu/~hellis/CS490/Homework%203%20fa14.pdf), Western New England College.*


## Project Summary 

Provide a written summary discussing what you found and what you thought about the project. Your summary should answer the following questions about the project: 

* What is the purpose of the project?

* How many people are working on the project?

* How active is the project?

* Where would you start if you were interested in helping with the project?

* What usable technical documents that describe the project (e.g., functional requirements, design documents, installation documentation, etc.) exist?

* How are bugs and feature requests tracked? 
    * Are there a lot of open items? 
    * Are they being worked on? 
    * Are issues being addressed?

* How long do you think it would take you to download and install this product?

* How do the developers communicate with each other? What channels or tools are available within the project?

* What has been the most interesting thing you learned through this exercise?

## Submission

