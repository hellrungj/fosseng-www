---
---

I will have shared a contract for you to read, consider, and discuss (as needed). Our intent will be to ratify. If significant changes are needed, we will ratify later in the week, after edits have been made and reconsideration has been undertaken.
