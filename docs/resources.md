---
title: Resources
resource: local
---

## Resources

* **[Readings](https://berea.box.com/softeng-readings)**

    Our shared readings folder on Box.

## Tools

* **[FOSSEng @ Slack](https://fosseng.slack.com/)**

    Our Slack team.