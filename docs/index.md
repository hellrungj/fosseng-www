---
layout: default
title: Resources
---

These are articles, documents, and web pages that you might find of use or interest throughout the term. Some will be explicitly referenced; others are here for your enjoyment.

{% for section in site.data.resources %}

<h3> {{section.section}} </h3>

<table id="local" class="table table-striped table-hover">
<tbody>
{% for res in section.links %}
<tr>
<td width="10%"> {% include fa name=res.icon %} </td>
<td><a href="{{res.link}}">{{res.title}}</a></td>
</tr>
{% endfor %}
</tbody>
</table>

{% endfor %}
