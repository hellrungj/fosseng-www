UNAME := $(shell uname -s)
ifeq ($(UNAME),Linux)
	PORT=4000
	IP=0.0.0.0
endif
ifeq ($(UNAME),Darwin)
	PORT=4000
	IP=localhost
endif

local:
	jekyll serve -w --host ${IP} --port ${PORT} --config _config.yml,_config_local.yml

remote:
	jekyll build --config _config.yml,_config_remote.yml
	#python ./_cleanup_ics.py
	rsync -p --chmod=Fa+r -vrz \
		-e "ssh -i ${HOME}/.ssh/big-mac-berea"  _site/ \
		jadudm@jadud.com:~/jadud.com/teaching/softeng-sp16
test:
	jekyll build --config _config.yml,_config_remote.yml
	python ./_cleanup_ics.py
