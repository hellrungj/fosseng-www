;; This is written in the full Racket language.
;; It includes the beginner language so it can be used
;; with HTDP 2/e.
#lang racket
(require rackunit rackunit/text-ui)
(require lang/htdp-beginner)

;; I'll use this definition so tests fail.
;; Replace it with a correct expression in the questions below.
(define FIXME 'FIXME)

;; Some strings we can work with
(define keep-brain
  "Never trust anything that can think for itself if you can't see where it keeps its brain!")

(define non-working-cat
  "If you try and take a cat apart to see how it works, the first thing you have on your hands is a nonworking cat.")

(define you-nique
  "Always remember that you are absolutely unique. Just like everyone else.")

;; The following should use functions you saw described in 
;; section 1.2 of HTDP 2/e.

;; Extract the eigth element of the cat quote.
(define s1 FIXME)

;; Calculate the length of all three strings, summed.
(define s2 FIXME)

;; Combine the first and third quotes with a space in-between.
(define s3 FIXME)

;; Create a new quote:
;; "Never see everyone"
;; You should do this by extracting the appropriate
;; substrings from the first, second, and third quotes,
;; and appending them together into a single string.
;; The appropriate spaces should be part of the substrings.
(define s4 FIXME)

;; Tests
(define-test-suite string-suite
  (check-equal? s1 "r")
  (check-equal? s2 273)
  (check-equal? s3 "Never trust anything that can think for itself if you can't see where it keeps its brain! Always remember that you are absolutely unique. Just like everyone else.")
  (check-equal? s4 "Never see anyone")
  )

(run-tests string-suite)

